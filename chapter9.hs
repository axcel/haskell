import Data.Char

addBang [] = []
addBang (x:xs) = (x ++ "!") : addBang xs

squareAll [] = []
squareAll (x:xs) = x^2 : squareAll xs

myMap f [] = []
myMap f (x:xs) = (f x) : myMap f xs

-- filter even [1,2,3,4]
-- filter (\(x:xs) -> x == 'a') ["abc", "cde", "aab"]

myFilter test [] = []
myFilter test (x:xs) = if test x
                       then x:myFilter test xs
                       else myFilter test xs

myRemove test (x:xs) = if test x
                        then myRemove test xs
                        else x:myRemove test xs

-- foldl (+) 0 [1,2,3,4]

concatAll xs = foldl (++) "" xs

myProduct xs = foldl (*) 1 xs

sumOfSquares xs = foldl (+) 0 (map (^2) xs)

rcons xs y = y:xs
myReverse xs = foldl rcons [] xs

myFoldl f init [] = init
myFoldl f init (x:xs) = myFoldl f newInit xs
    where newInit = f init x

myFoldr f init [] = init
myFoldr f init (x:xs) = f x rightResult
    where rightResult = myFoldr f init xs


myElem val myList = (length filteredList) /= 0
    where filteredList = filter (== val) myList


isPalindrome text = processedText == reverse processedText
    where noSpaces = filter (/= ' ') text 
          processedText = map toLower noSpaces

harmonical n = sum (take n seriesValues)
    where seriesPairs = zip (cycle [1.0]) [1.0,2.0 .. ]
          seriesValues = map (\pair -> (fst pair)/(snd pair))
                             seriesPairs