getRequestUrl host apiKey resource id =
    host ++ "/" ++ resource ++ "/" ++ id ++ "?token=" ++ apiKey


genHostRequestBuilder host = (\apiKey resource id ->
        getRequestUrl host apiKey resource id)

genApiRequestBuilder hostBuilder apiKey = (\resource id ->
        hostBuilder apiKey resource id)

genResourceRequestBuilder hostBuilder apiKeyBuilder resource = (\id ->
        apiKeyBuilder resource id)

exmapleUrlBuilder :: [Char] -> [Char] -> [Char] -> [Char]
exmapleUrlBuilder = genHostRequestBuilder "http://example.com"

myExampleUrlBuilder = genApiRequestBuilder exmapleUrlBuilder "123hasklll"
