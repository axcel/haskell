sfOffice name = 
    if lastName < "Л"
    then nameText ++
        " - А/я 1234, Сан-Франциско, штат Калифорния, 94111"
    else nameText ++
        " - А/я 1010, Сан-Франциско, штат Калифорния, 941109"
    where lastName = snd name
          nameText = (fst name) ++ " " ++ lastName

nyOffice name = nameText ++ 
    ": А/я 789, Нью-Йорк, штат Нью-Йорк, 10013"
    where nameText = (fst name) ++ " " ++ (snd name)

renoOffice name = nameText ++ " - А/я 456, Рино, штат Невада, 89523"
    where nameText  = snd name

getLocationFunction location =
    case location of
        "ny" -> nyOffice
        "sf" -> sfOffice
        "reno" -> renoOffice
        _ -> (\name ->
                (fst name) ++ " " ++ (snd name))

addressLetter name location = locationFunction name
    where locationFunction = getLocationFunction location

addressLetterV2 location name = addressLetter name location

flipBinaryArgs binaryFunction = (\x y -> binaryFunction y x)

addressLetterV2flip = flipBinaryArgs addressLetter